var PolynomialLine = require('./PolynomialLine').PolynomialLine;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;

/**
 * Natężenie prądu
 */
class Amperage extends PolynomialLine {
    constructor(name, power_on_shaft, voltage, engine_power_factor, efficiency_3_4, number_of_fans = 1, createGraphLine = true) {
        super(name, null, createGraphLine);
        this.power_on_shaft = power_on_shaft;
        this.voltage = voltage;
        this.engine_power_factor = engine_power_factor;
        this.engine_efficiency_3_4 = efficiency_3_4;
        this.number_of_fans = number_of_fans;
    }

    fun(x) {
        return (1000 * (this.power_on_shaft.fun(x) / (this.voltage * Math.sqrt(3) * this.engine_power_factor * this.engine_efficiency_3_4))) * this.number_of_fans;
    }

    get polynomial() {
        let coefficients = this.power_on_shaft.polynomial.coefficients.slice(0);//klonowanie
        coefficients[0] = (coefficients[0]  / (this.voltage * Math.sqrt(3) * this.engine_power_factor * this.engine_efficiency_3_4)) * 1000;
        coefficients[1] = (coefficients[1]  / (this.voltage * Math.sqrt(3) * this.engine_power_factor * this.engine_efficiency_3_4)) * 1000;
        coefficients[2] = (coefficients[2]  / (this.voltage * Math.sqrt(3) * this.engine_power_factor * this.engine_efficiency_3_4)) * 1000;
        coefficients[3] = (coefficients[3]  / (this.voltage * Math.sqrt(3) * this.engine_power_factor * this.engine_efficiency_3_4)) * 1000;
        return new Polynomial(coefficients);
    }

    get polynomialOryginal() {
        let coefficients = this.power_on_shaft.polynomialOriginal.coefficients.slice(0);//klonowanie
        coefficients[0] = (coefficients[0]  / (this.voltage * Math.sqrt(3) * this.engine_power_factor * this.engine_efficiency_3_4)) * 1000;
        coefficients[1] = (coefficients[1]  / (this.voltage * Math.sqrt(3) * this.engine_power_factor * this.engine_efficiency_3_4)) * 1000;
        coefficients[2] = (coefficients[2]  / (this.voltage * Math.sqrt(3) * this.engine_power_factor * this.engine_efficiency_3_4)) * 1000;
        coefficients[3] = (coefficients[3]  / (this.voltage * Math.sqrt(3) * this.engine_power_factor * this.engine_efficiency_3_4)) * 1000;
        return new Polynomial(coefficients);
    }

    calcMinMax(minX, maxX) {
        let minValue = null;
        let maxValue = null;

        let xPositions = [];

        let length = maxX - minX;
        let n = 5;

        let gap = length / (n - 1);
        let elementN = minX;

        xPositions.push(minX);

        for (let i = 0; i < n - 1; i++) {
            elementN += gap;
            xPositions.push(elementN);
        }

        for (let xPosition of xPositions) {
            let calculatedValue = this.fun(xPosition);

            if (!minValue || minValue > calculatedValue)
                minValue = calculatedValue;

            if (!maxValue || maxValue < calculatedValue)
                maxValue = calculatedValue;
        }

        return {min: minValue, max: maxValue};
    }

    getZeroPoint(min) {
        return this.polynomialOryginal.calcZeroPoints().sort((a, b) => a - b).find(a => a > min);
    }
}

module.exports.Amperage = Amperage;