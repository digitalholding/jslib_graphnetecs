require("prototype-extensions/js/DomExtensions")

class Bar {
    constructor(config) {
        this.container = document.create('div', {className: 'barGraph__bar'})
        this.config = config
        this.value = this.config.value
        this._color = this.config.color

        this.drawValue()
        this.drawLabel()
    }

    set _height(value) { this.container.style.height = `${value}px` }
    set _labelFontSize(value) { this.labelElem.style.fontSize = `${value}px` }
    set _labelSpace(value) { 
        this.labelElem.style.bottom = `-${value}px` 
        this.valueElem.style.top = `-${value}px`
    }
    set _color(value) {
        if(this.value) {
            if(value) this.container.style.backgroundColor = value
            else this.container.classList.add('bordered')
        }
    }

    get html() { return this.container }

    drawValue() {
        this.valueElem = document.create('span', {className: 'barGraph__bar-value', text: this.value})
        this.container.appendChild(this.valueElem)
    }

    drawLabel() {
        this.labelElem = document.create('span', {className: 'barGraph__bar-label', text: this.config.label})
        this.container.appendChild(this.labelElem)
    }
}

module.exports.Bar = Bar