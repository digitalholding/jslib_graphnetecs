require("prototype-extensions/js/DomExtensions")
const Plane = require('./Plane').Plane

class Graph {
    constructor(data, config) {
        this.container = document.create('div', {className: 'barGraph__graph'})
        this.config = config
        this.data = data
        this.planeConfig = {
            height: this.config.plane.height,
            bars: this.data
        }
        this.plane = new Plane(this.planeConfig)

        this.draw()
        addEventListener('resize', this.draw.bind(this))
    }

    get html() { return this.container }

    draw() {
        this.container.innerHTML = ''
        let title = document.create('div', {className: 'barGraph__graph-title'})
        this.container.appendChild(title)
        this.config.title.forEach(elem => {
            title.appendChild(document.create('div', {html: elem}))
        })
        let graphContainer = document.create('div', {className: 'barGraph__graph-container'})
        this.container.appendChild(graphContainer)
        let yLabel = document.create('div', {className: 'barGraph__graph-y-label'})
        graphContainer.appendChild(yLabel)
        this.config.yLabel.forEach(elem => {
            yLabel.appendChild(document.create('div', {html: elem}))
        })
        let graph = document.create('div', {className: 'barGraph__graph-plane'})
        graphContainer.appendChild(graph)
        this.planeConfig.parent = graph
        graph.appendChild(this.plane.html)
        this.container.appendChild(document.create('div', {className: 'barGraph__graph-x-label', html: this.config.xLabel}))
        if(this.config.legend)
            this.container.appendChild(document.create('div', {className: 'barGraph__graph-legend', html: this.config.legend}))
    }
}

module.exports.Graph = Graph