require("prototype-extensions/js/DomExtensions")
const Bar = require('./Bar').Bar

class Plane {
    constructor(config) {
        this.config = config
        this.container = document.create('div', {className: 'barGraph__plane'})
        this.bars = []
        this.config.bars.forEach(bar => { this.bars.push(new Bar(bar)) })
        this._labelSpace = 30

        this.draw()
        addEventListener('resize', this.draw.bind(this))
    }

    set _width(value) { this.container.style.width = (value) ? `${value}px` : '100%' }
    set _height(value) { this.container.style.height = (value) ? `${value}px` : '100%' }
    set _labelSpace(value) { this.container.style.paddingBottom = `${value}px` }

    get _width() { return this.container.offsetWidth }
    get _height() { return parseInt(this.container.style.height) }
    get _labelSpace() { return parseInt(this.container.style.paddingBottom) }
    get html() { return this.container }
    get max() {
        return this.bars.reduce(function(prev, curr) {
            return (prev.value > curr.value) ? prev : curr
        })
    }

    draw() {
        this.container.innerHTML = ''
        this._width = (this.config.width) ? this.config.width : false
        this._height = (this.config.height) ? this.config.height : false
        this.bars.forEach(bar => {
            bar._height = bar.value * (this._height - this._labelSpace * 2) / this.max.value
            bar._labelSpace = this._labelSpace
            bar._labelFontSize = ((this._width / this.bars.length - 20) / 3.5 < 22) ? (this._width / this.bars.length - 20) / 3.5 : 22
            this.container.appendChild(bar.html)
        })
    }
}

module.exports.Plane = Plane