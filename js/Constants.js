let Constants = {
    frequency: 50,//częstotliwość, przy której robione są pomiary
    temperature: 293.15,//temperatura w kelwinach, przy mktórej robione są pomairy
    airDensity: 1.2,//gęstość powierrza, przy mktórej robione są pomairy
    airPressure: 101325,//ciśnienie powietrza w Pa, przy mktórej robione są pomairy
    specificGasConstant: 287.058,
};
module.exports.Constants = Constants;