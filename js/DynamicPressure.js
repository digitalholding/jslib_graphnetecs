const Constants = require('./Constants').Constants;

var Line = require('./Line').Line;
var GraphLine = require('graph/js/Line').Line;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;

/**
 * Ciśnienie dynamiczne
 */
class DynamicPressure extends Line {
    constructor(name, number_of_fans = 1, createGraphLine = true) {
        super(createGraphLine);
        this.name = name;
        this.area = 0;
        this.line = new GraphLine(this.fun.bind(this), null, createGraphLine);
        this.number_of_fans = number_of_fans;
    }

    get polynomial() {
        return this.polynomialOryginal;
    }

    get polynomialOryginal() {
        return new Polynomial([0, 0, (this.density / Math.pow((this.area), 2)) / 2]);
    }

    fun(x) {
        return (this.density / (Math.pow(this.area, 2) * 2)) * (x / this.number_of_fans) * (x / this.number_of_fans);
    }


    calcMinMax(minX, maxX) {
        let minValue = null;
        let maxValue = null;

        let xPositions = [];

        let length = maxX - minX;
        let n = 5;

        let gap = length / (n - 1);
        let elementN = minX;

        xPositions.push(minX);

        for (let i = 0; i < n - 1; i++) {
            elementN += gap;
            xPositions.push(elementN);
        }

        for (let xPosition of xPositions) {
            let calculatedValue = this.fun(xPosition);

            if (!minValue || minValue > calculatedValue)
                minValue = calculatedValue;

            if (!maxValue || maxValue < calculatedValue)
                maxValue = calculatedValue;
        }

        return {min: minValue, max: maxValue};
    }

    getZeroPoint(min) {
        return this.polynomialOryginal.calcZeroPoints().sort((a, b) => a - b).find(a => a > min);
    }
}

module.exports.DynamicPressure = DynamicPressure;