var GraphLine = require('graph/js/Line').Line;
var PolynomialLine = require('./PolynomialLine').PolynomialLine;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;
var Line = require('./Line').Line;

/**
 * Sprawność
 */
class EfficiencyLine extends Line {
    constructor(name, pressure, power, number_of_fans = 1, createGraphLine = true) {
        super(createGraphLine);
        this.name = name;
        this.pressure = pressure;
        this.power = power;
        this.line = new GraphLine(this.fun.bind(this), null, createGraphLine);
        this.number_of_fans = number_of_fans;
    }

    fun(x) {
        return (this.pressure.fun(x) * (x / this.number_of_fans) / (1000 * this.power.fun(x))) * this.number_of_fans;
    }

    /**
     * Oblicza punkt który jest najniżej (y najmniejszy) i najwyżej
     * Używany do skalowania wykresu i do obliczania punktu najlepszej sprawności
     * zwraca 2 punkty, zarówno ich pozycje na osi X i Y
     * @param start początek wykresu na osi X
     * @param end koniec wykresu na osi X
     * @returns {{min: *, maxVal: *, minVal: *, max: *}}
     * @private
     */
    _calcExtremums(start, end) {
        //liczymy pochodną i jej miejsca zerowe
        //sprawność jest funkcją wymierną (wielomain dzielony przez wielomian)

        let nominator = this.pressure.polynomial.multiply(new Polynomial([0, 1]));//licznik
        let denominator = this.power.polynomial.multiply(new Polynomial([1000]));//mianownik
        let nominatorOfDerriverance = nominator.derriverance.multiply(denominator).subtract(nominator.multiply(denominator.derriverance));//licznik pochodnej. Szukamy miejsc zerowych, więc licznik wystarczy

        let startValue = this.fun(start);
        let max, maxVal, min, minVal;
        max = min = start;
        maxVal = minVal = startValue;

        let zeroPoints = nominatorOfDerriverance.calcZeroPointsRange(start, end);//zwraca wszystkie miejsca zerowe pochodnej, czyli potencjalne punkty minimalne i maksymalne
        zeroPoints.push(start);//sprawdzamy też początek
        zeroPoints.push(end);//i koniec funkcji
        for (let x of zeroPoints) {
            let value = this.fun(x);
            if (value > maxVal) {
                maxVal = value;
                max = x;
            }
            if (value < minVal) {
                minVal = value;
                min = x;
            }
        }
        return {min, max, minVal, maxVal};
    }

    /**
     * Zwraca maksymalny (o największym Y) punkt w danym przedziale
     * @param start
     * @param end
     * @returns {{x: *, y: *}}
     */
    findMaximum(start, end) {
        let {max, maxVal} = this._calcExtremums(start, end);
        return {x: max, y: maxVal};
    }

    /**
     * Zwraca zakres funkcji na osi Y (czyli najmniejszy i największy Y)
     * Używany do skalowania wykresu
     * @param minX
     * @param maxX
     * @returns {{min: *, max: *}}
     */
    // calcMinMax(minX, maxX) {
    //     let {minVal, maxVal} = this._calcExtremums(minX, maxX);
    //     return {min: minVal, max: maxVal};
    // }

    calcMinMax(minX, maxX) {
        let minValue = null;
        let maxValue = null;

        let xPositions = [];

        let length = maxX - minX;
        let n = 5;

        let gap = length / (n - 1);
        let elementN = minX;

        xPositions.push(minX);

        for (let i = 0; i < n - 1; i++) {
            elementN += gap;
            xPositions.push(elementN);
        }

        for (let xPosition of xPositions) {
            let calculatedValue = this.fun(xPosition);

            if (!minValue || minValue > calculatedValue)
                minValue = calculatedValue;

            if (!maxValue || maxValue < calculatedValue)
                maxValue = calculatedValue;
        }

        return {min: minValue, max: maxValue};
    }

    getDataByY(y, min, max) {

        let nominator = this.pressure.polynomial.multiply(new Polynomial([0, 1]));
        let denominator = this.power.polynomial.multiply(new Polynomial([1000]));
        let probableResults = nominator.subtract(denominator.multiply(new Polynomial([y]))).calcZeroPointsRange(min, max);

        return probableResults.filter(x => denominator.calc(x) != 0);
    }
}

module.exports.EfficiencyLine = EfficiencyLine;