var PolynomialLine = require('./PolynomialLine').PolynomialLine;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;

/**
 * Natężenie prądu
 */
class ElectricalPower extends PolynomialLine {
    constructor(name, power_on_shaft, efficiency_3_4, number_of_fans = 1, createGraphLine = true) {
        super(name, null, createGraphLine);
        this.power_on_shaft = power_on_shaft;
        this.engine_efficiency_3_4 = efficiency_3_4;
        this.number_of_fans = number_of_fans;
    }

    fun(x) {
        return (this.power_on_shaft.fun(x) / this.engine_efficiency_3_4);
    }

    get polynomial() {
        let coefficients = this.power_on_shaft.polynomial.coefficients.slice(0);//klonowanie
        coefficients[0] = coefficients[0] / this.engine_efficiency_3_4;
        coefficients[1] = coefficients[1] / this.engine_efficiency_3_4;
        coefficients[2] = coefficients[2] / this.engine_efficiency_3_4;
        coefficients[3] = coefficients[3] / this.engine_efficiency_3_4;
        return new Polynomial(coefficients);
    }

    get polynomialOryginal() {
        let coefficients = this.power_on_shaft.polynomialOriginal.coefficients.slice(0);//klonowanie
        coefficients[0] = coefficients[0] / this.engine_efficiency_3_4;
        coefficients[1] = coefficients[1] / this.engine_efficiency_3_4;
        coefficients[2] = coefficients[2] / this.engine_efficiency_3_4;
        coefficients[3] = coefficients[3] / this.engine_efficiency_3_4;
        return new Polynomial(coefficients);
    }

    calcMinMax(minX, maxX) {
        let minValue = null;
        let maxValue = null;

        let xPositions = [];

        let length = maxX - minX;
        let n = 5;

        let gap = length / (n - 1);
        let elementN = minX;

        xPositions.push(minX);

        for (let i = 0; i < n - 1; i++) {
            elementN += gap;
            xPositions.push(elementN);
        }

        for (let xPosition of xPositions) {
            let calculatedValue = this.fun(xPosition);

            if (!minValue || minValue > calculatedValue)
                minValue = calculatedValue;

            if (!maxValue || maxValue < calculatedValue)
                maxValue = calculatedValue;
        }

        return {min: minValue, max: maxValue};
    }

}

module.exports.ElectricalPower = ElectricalPower;