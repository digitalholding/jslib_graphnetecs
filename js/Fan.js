const EfficiencyLine = require('./EfficiencyLine').EfficiencyLine;
const Pressure = require('./Pressure').Pressure;
const PowerOnShaft = require('./PowerOnShaft').PowerOnShaft;
const StaticPressure = require('./StaticPressure').StaticPressure;
const StaticPressureIncrease = require('./StaticPressureIncrease').StaticPressureIncrease;
const DynamicPressure = require('./DynamicPressure').DynamicPressure;
const Amperage = require('./Amperage').Amperage;
const ElectricalPower = require('./ElectricalPower').ElectricalPower;

/**
 * Obiekt tej klasy przedstawia zbiór wszystkich charakterystyk jednego pomiaru
 * W panelu admina mamy 2 obiekty tej klasy: charakterystyka pierwotna (szara) i przeliczona
 */
class Fan {
    constructor() {
        this.lines = {};
        this.min = 0;
        this.max = 1;
        this.number_of_fans = 1;
    }

    /**
     * generuje linie, które są obliczane automatycznie (najpierw muszą być uzupełnione linie z wielomianów wpisywanych ręcznie)
     */
    generateLines(createGraphLine = true) {
        this.lines.PowerOnShaftWithMaterial = new PowerOnShaft('PowerOnShaftWithMaterial', this.lines.PowerOnShaft.polynomial.coefficients.slice(), null, this.number_of_fans, createGraphLine);
        this.lines.TotalEfficiency = new EfficiencyLine('TotalEfficiency', this.lines.TotalPressure, this.lines.PowerOnShaft, this.number_of_fans, createGraphLine);
        this.lines.StaticPressure = new StaticPressure('StaticPressure', this.lines.TotalPressure, 0, this.number_of_fans, createGraphLine);
        this.lines.StaticPressureIncrease = new StaticPressureIncrease('StaticPressureIncrease', this.lines.TotalPressure, 1, 1, this.number_of_fans, createGraphLine);
        this.lines.StaticEfficiency = new EfficiencyLine('StaticEfficiency', this.lines.StaticPressure, this.lines.PowerOnShaft, this.number_of_fans, createGraphLine);
        this.lines.DynamicPressure = new DynamicPressure('DynamicPressure', this.number_of_fans, createGraphLine);
        this.lines.Amperage = new Amperage('Amperage', this.lines.PowerOnShaft, 0,0,0, this.number_of_fans, createGraphLine);
        this.lines.AmperageV2 = new Amperage('AmperageV2', this.lines.PowerOnShaftWithMaterial, 0,0,0, this.number_of_fans, createGraphLine);
        this.lines.ElectricalPower = new ElectricalPower('ElectricalPower', this.lines.PowerOnShaftWithMaterial, 0, this.number_of_fans, createGraphLine);
        this.lines.DynamicPressure.line.config.color = '#00aa00';
    }
}

module.exports.Fan = Fan;