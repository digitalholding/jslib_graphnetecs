if (!window.testing) {
    require('!style-loader!css-loader!sass-loader!graph/css/scss/main.scss');
    require('!style-loader!css-loader!sass-loader!../css/scss/main.scss');
}
require('!style-loader!css-loader!sass-loader!graph/css/scss/main.scss');
require('!style-loader!css-loader!sass-loader!../css/scss/main.scss');

var GraphPlane = require('graph/js/Plane').Plane;
var GraphAxis = require('graph/js/Axis').Axis;
var VerticalMeasureLine = require('./VerticalMeasureLine').VerticalMeasureLine;
var HorizontalMeasureLine = require('./HorizontalMeasureLine').HorizontalMeasureLine;

/**
 * Odpowiada za pojedynczy wykres, zarówno zbiorczy jak i pojedynczy
 */
class GraphNetecs {
    constructor(units, createGraphLine = true) {
        this.units = units;
        this.createGraphLine = createGraphLine;
        /**
         * Obiekt typu Plane z biblioteki graph
         * Sam ten obiekt rysuje wykresy, ale nie zawiera nic typowego dla netecsu
         * @type {Plane}
         */
        this.graph = new GraphPlane(null, createGraphLine);
        this.graph.graphNetecs = this;
        this.graph.config = {
            mouseProjection: true,
            tooltipPointEnabled: true,
            tooltipPointFunction: this.tooltipPointFunction.bind(this),
            y: {startLimit: 0, step: 50, smartScale: true, thickness: 50, marginStart: 10, marginEnd: 110},
            x: {
                startLimit: 0,
                step: 50,
                titleHTML: '<math><msub><mn>q</mn><sub>V</sub></msub></mn>',
                smartScale: true,
                marginStart: 10,
                marginEnd: 85,
                unit: units.performance.list[0]
            }
        };
        this._measureLines = [];
        this._lines = [];
        this._points = [];
        this.minX = 0;
        this.maxX = 1;
        this.minXOrg = 0;
        this.maxXOrg = 1;

        this.multipleAxis = false;//Zezwalaj na wiele osi (np. wykres zbiorczy)
        this._axis = {Pa: this.graph.ys};//zawiera osie pionowe (ważne, gdy multipleAxis == true)
    }

    /**
     * czy zmiana skali wykresu jest zablokowana
     * blokujemy gdy np. ktoś myszą coś przeciąga (wtedy skala się dopasuje po puszczeniyu myszy) 
     * @returns {*}
     */
    isRescalingBlocked() {
        return this.graph.svg.matches(':active');
    }

    /**
     * Tekst wyświetlający się przy myszy gdy najedziemy na wykres
     * @param x
     * @param y
     * @returns {HTMLElement}
     */
    tooltipPointFunction(x, y) {
        if(this.createGraphLine) {
            let element = document.createElement('div');
            let xEl = document.createElement('div');
            let yEl = document.createElement('div');
            xEl.innerHTML = this.graph.xs.getConfigValue('titleHTML') + "= " + x.display.format(3);
            yEl.innerHTML = this.graph.ys.getConfigValue('titleHTML') + "= " + y.display.format(3);
            element.appendChild(xEl);
            element.appendChild(yEl);
            return element;
        }else{
            return null;
        }
    }

    get measureLines() {
        return this._measureLines;
    }

    /**
     * obiekt DOM całego wykresu. należy go dodać gdzieś do strony (np. appendChild)
     * @returns {HTMLElement }
     */
    get html() {
        if (!this._html && this.createGraphLine) {
            this._makeHtml();
        }
        return this._html;
    }

    /**
     * Wszystkie linie (charakterystyki) na wykresie
     * @returns {*[]}
     */
    get lines() {
        return this._lines.slice()
    }

    /**
     * Punktty na wykresie
     * @returns {*[]}
     */
    get points() {
        return this._points.slice()
    }

    _makeHtml() {
        this._html = document.createElement("div");
        this._html.appendChild(this.graph.svg);
        this.graph.svg.setAttribute('width', '100%');
        this.graph.svg.setAttribute('height', '100%');
    }

    addLine(line) {
        this._lines.push(line);
        this.graph.add(line.line)
        if (line.showStartEndPoints) {
            line.startPoint.ys = line.line.ys;
            line.endPoint.ys = line.line.ys;
            this.graph.add(line.startPoint);
            this.graph.add(line.endPoint);
        }
    }

    removeLine(line) {
        let index = this._lines.indexOf(line);
        if (index < 0) {
            return false;
        }
        this._lines.splice(index, 1);
        this.graph.remove(line.line)
        if (line.showStartEndPoints) {
            this.graph.remove(line.startPoint)
            this.graph.remove(line.endPoint)
        }
        return true;
    }

    addPoint(point) {
        this._points.push(point);
        this.graph.add(point)
    }

    removePoint(point) {
        let index = this._points.indexOf(point);
        if (index < 0) {
            return false;
        }
        this._points.splice(index, 1);

        this.graph.remove(point)

        return true;
    }

    draw() {
        if (!this.isRescalingBlocked()) {
            let {min, max} = this._setMinMaxX();
            if (this.multipleAxis)
                this._setMinMaxYMultipleAxis(min, max);
            else
                this._setMinMaxY(min, max);
        }
        this.graph.draw();
    }

    /**
     * ustawia zakres wykresu na osi X
     * @returns {{min: *, max: *}}
     * @private
     */
    _setMinMaxX() {
        let xs = this._measureLines.filter(line => line instanceof VerticalMeasureLine).map(line => line.x)
        xs.push(this.minX);
        xs.push(this.minXOrg);
        xs.push(this.maxX);
        xs.push(this.maxXOrg);
        this.graph.xs.config = {
            start: Math.min.apply(null, xs),
            end: Math.max.apply(null, xs)
        };
        return {min: this.graph.xs.start, max: this.graph.xs.end};
    }

    /**
     * ustawia zakres wykresu dla osi Y gdy mamy tylko jedną taką oś
     * @private
     */
    _setMinMaxY() {
        let ys = this._measureLines.filter(line => line instanceof HorizontalMeasureLine).map(line => line.y)
        for (let line of this._lines) {
            if (!line.chartScaling) continue;
            let {min: fmin, max: fmax} = line.calcMinMax(line.min, line.max);
            if (isFinite(fmin)) {
                ys.push(fmin);
            }
            if (isFinite(fmax)) {
                ys.push(fmax);
            }
        }
        let min = Math.min.apply(null, ys)
        let max = Math.max.apply(null, ys)
        if (!isFinite(min)) {
            min = 0;
        }
        if (!isFinite(max)) {
            max = min + 1;
        }
        this.graph.ys.config = {start: Math.max(min, 0), end: max};
    }

    /**
     * zwraca nam oś Y na podstawie typu np. pressure, noise
     * @param kind
     * @returns {*}
     */
    getAxisByKind(kind) {
        if (this._axis[kind]) {
            return this._axis[kind];
        } else {
            let newAxis = new GraphAxis('y');
            return this._axis[kind] = newAxis;
        }
    }

    /**
     * ustawia zakres wykresu dla osi Y gdy mamy wiele osi (wykres zbiorczy)
     * jest bardziej skomplikwoana, bo każdą oś trzeba skalwoać osobno
     * @param minX
     * @param minY
     * @private
     */
    _setMinMaxYMultipleAxis(minX, minY) {
        for (let line of this._lines) {
            line.line.ys = this.getAxisByKind(line.kind);
        }
        let countAxis = 0;
        for (let axis of Object.values(this._axis)) {
            let min = Number.POSITIVE_INFINITY;
            let max = Number.NEGATIVE_INFINITY;
            for (let line of this._lines) {
                if (!line.chartScaling) continue;
                if (line.line.ys != axis) continue;
                let {min: fmin, max: fmax} = line.calcMinMax(this.minX, this.maxX);
                if (fmin < min) {
                    min = fmin;
                }
                if (fmax > max) {
                    max = fmax;
                }
            }
            if (isFinite(min) && isFinite(max)) {
                countAxis++;
                if (!this.graph.containsAxis(axis))
                    this.graph.addAxis(axis);
            } else {
                this.graph.removeAxis(axis);
            }
            axis.config = {start: Math.max(min, 0), end: max};
        }
        if (countAxis == 0) {
            this._axis.Pa.config = {strat: 0, end: 1};
            this.graph.addAxis(this._axis.Pa);
        }
    }

    addMeasureLine(line) {
        this._measureLines.push(line);
        this.graph.add(line);
    }

    removeMeasureLine(line) {
        this._measureLines.splice(this._measureLines.indexOf(line), 1);
        this.graph.remove(line);
    }

    containsLine(line) {
        return this._lines.indexOf(line) >= 0;
    }

    containsPoint(point) {
        return this._points.indexOf(point) >= 0;
    }

    removeAll() {
        while (this._lines.length > 0) {
            this.removeLine(this._lines[0]);
        }
        while (this._points.length > 0) {
            this.removePoint(this._points[0]);
        }
    }

    /**
     * zwraca dla jakich X mamy daną wartość Y
     * używane np. w odciętych
     * @param y
     * @returns {Array}
     */
    getDataByY(y) {
        let ret = [];
        this.lines.forEach(line => {
            let x = line.getDataByY(y, line.min, line.max).map(value => this.graph.xs.createValue(value));
            y = this.graph.ys.createValue(y * 1);
            ret.push({line, y, x});
        });
        return ret;
    }
}

module.exports.GraphNetecs = GraphNetecs;