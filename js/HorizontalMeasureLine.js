var MeasureLine = require('./MeasureLine').MeasureLine;
const ObjectEvent = require('object-event').ObjectEvent;

class HorizontalMeasureLine extends MeasureLine {
    constructor(value, allLines = []) {
        super(0, value);
        this._allLines = allLines;
        allLines.push(this);
        this.changed = ObjectEvent('changed');
        this.moved(e => {
            this.y = e.y;
        });
    }

    get y() {
        return this._y;
    }

    set y(value) {
        if (this._allLines) {
            for (let line of this._allLines) {
                line._y = value;
                line.changed.call(line);
            }
        } else {//w konstruktorze
            this._y = value;
        }
    }

    _draw(xs, ys, width, height) {
        super._draw(xs, ys, width, height);

        if (!this._line) {
            this._line = document.createElementNS("http://www.w3.org/2000/svg", 'line');
            this._line.classList.add('measureLine');
            this._line.classList.add('horizontalMeasureLine');
            this._line.style.stroke = this._color;
            this.svg.appendChild(this._line);
        }

        let yPX = ys.getPixels(this.y);

        this._line.setAttribute('x1', xs.marginStart);
        this._line.setAttribute('y1', yPX);
        this._line.setAttribute('x2', width - xs.marginEnd);
        this._line.setAttribute('y2', yPX);

    }
}

module.exports.HorizontalMeasureLine = HorizontalMeasureLine