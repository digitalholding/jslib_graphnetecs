var GraphPoint = require('graph/js/Point').Point;

class InstalledPower extends GraphPoint {
    constructor(value) {
        super(value, 0);
    }

    get x() {
        return this._x;
    }

    set x(value) {
        this._x = value;
    }

    get color() {
        return this._color || '';
    }

    set color(value) {
        this._color = value;
        if (this._line)
            this._line.style.stroke = value;
    }

    _draw(xs, ys, width, height) {
        super._draw(xs, ys, width, height);

        if (!this._line) {
            this._line = document.createElementNS("http://www.w3.org/2000/svg", 'line');
            this._line.classList.add('installedPower');
            this._line.style.stroke = this._color;
            this.svg.appendChild(this._line);
        }

        let xPX = xs.getPixels(this.x);

        this._line.setAttribute('x1', xPX);
        this._line.setAttribute('y1', ys.marginEnd);
        this._line.setAttribute('x2', xPX);
        this._line.setAttribute('y2', height - ys.marginStart);
    }
}

module.exports.InstalledPower = InstalledPower;