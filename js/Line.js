var GraphPoint = require('graph/js/Point').Point;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;

/**
 * klasa przedstawia pojedynczą funkcję (w sensie matematycznym) na wykrenie
 * NIE chodzi tu o linie typu osie, rzedne/odcięte, rzutowania itp.
 */
class Line {
    constructor(createGraphLine) {
        /**
         * true to charakterystyka która nas interesuje
         * false to linia wyszarzona (np. charakterystyka pierwotna)
         * @type {boolean}
         * @private
         */
        this._isActive = true;
        /**
         * czy ta linia ma być brana pod uwagę, gdy skalujemy wykres (to znaczy, czy musi się w całości zmieścić)
         * @type {boolean}
         */
        this.chartScaling = true;
        /**
         * Czy mamy punkt startowy i końcowy (one są i tak przezroczyte, ale title się wyświetla)
         * @type {boolean}
         */
        this.showStartEndPoints = true;

        if(createGraphLine)
            this._createPoints();
    }

    get isActive() {
        return this._isActive;
    }

    get kind() {
        let types = {
            TotalPressure: 'pressure',
            DynamicPressure: 'pressure',
            TotalEfficiency: 'efficiency',
            PowerOnShaft: 'power',
            PowerOnShaftWithMaterial: 'power',
            Noise: 'noise',
            StaticPressure: 'pressure',
            StaticPressureIncrease: 'pressure',
            StaticEfficiency: 'efficiency',
            Requested: 'pressure',
            Amperage: 'current',
            AmperageV2: 'current',
            ElectricalPower: 'power',
        };
        return types[this.name];
    }

    set isActive(value) {
        this._isActive = value;
        this.showStartEndPoints = value;
        if (value) {
            this.startPoint.svgPoint.style.display = '';
            this.endPoint.svgPoint.style.display = '';
        } else {
            this.startPoint.svgPoint.style.display = 'none';
            this.endPoint.svgPoint.style.display = 'none';
        }
    }

    get title() {
        let types = {
            TotalPressure: 'tooltip_total_pressure',
            DynamicPressure: 'tooltip_dynamic_pressure',
            TotalEfficiency: 'tooltip_total_efficiency',
            PowerOnShaft: 'tooltip_power_on_shaft',
            PowerOnShaftWithMaterial: 'tooltip_power_on_shaft_with_material',
            Noise: 'tooltip_noise',
            StaticPressure: 'tooltip_static_pressure',
            StaticPressureIncrease: 'tooltip_static_pressure_increase',
            StaticEfficiency: 'tooltip_static_efficiency',
            Amperage: 'tooltip_amperage_v1',
            AmperageV2: 'tooltip_amperage_v2',
            ElectricalPower: 'tooltip_electrical_power',
        };
        return types[this.name];
    }

    /**
     * nazwa symboliczna lini
     * @returns {string}
     */
    get symbol() {
        let types = {
            TotalPressure: 'p_F',
            DynamicPressure: 'p_D',
            TotalEfficiency: 'η_F',
            PowerOnShaft: 'P_a',
            PowerOnShaftWithMaterial: 'P_am',
            Noise: 'L_WA6',
            StaticPressure: 'p_SF',
            StaticPressureIncrease: 'p_SPR',
            StaticEfficiency: 'η_SF',
            Amperage: 'A',
            AmperageV2: 'A',
            ElectricalPower: 'P',
        };
        return types[this.name];
    }

    /**
     * nazwa symboliczna lini w wersji z indeksami dolnymi
     * @returns {string}
     */
    get symbolSVG() {
        let types = {
            TotalPressure: 'p<tspan baseline-shift = "sub">F</tspan>',
            DynamicPressure: 'p<tspan baseline-shift = "sub">D</tspan>',
            TotalEfficiency: 'η<tspan baseline-shift = "sub">F</tspan>',
            PowerOnShaft: 'P<tspan baseline-shift = "sub">a</tspan>',
            PowerOnShaftWithMaterial: 'P<tspan baseline-shift = "sub">am</tspan>',
            Noise: 'L<tspan baseline-shift = "sub">WA6</tspan>',
            StaticPressure: 'p<tspan baseline-shift = "sub">SF</tspan>',
            StaticPressureIncrease: 'p<tspan baseline-shift = "sub">SPR</tspan>',
            StaticEfficiency: 'η<tspan baseline-shift = "sub">SF</tspan>',
            Amperage: 'I',
            AmperageV2: 'I',
            ElectricalPower: 'P',
        };
        return types[this.name];
    }

    /**
     * Gdzie na osi poziomej (x) zaczyna się linia
     * @returns {number}
     */
    get min() {
        return this._min;
    }

    set min(min) {
        this._min = min;
        this.line.config.start = min;

    }

    /**
     * Gdzie na osi poziomej (x) kończy się linia
     * @returns {number}
     */
    get max() {
        return this._max;
    }

    set max(max) {
        this._max = max;
        this.line.config.end = max;

    }

    /**
     * Tworzy punkty końcowy i początkowy
     * @private
     */
    _createPoints() {
        if (this.startPoint) return;
        this.startPoint = new GraphPoint(0, 0);
        this.startPoint.config = {projection: false};
        this.startPoint.titleAsSVG = true;
        this.startPoint.titleLines = 2;
        this._createCircle(this.startPoint);
        Object.defineProperty(this.startPoint, 'x', {get: () => this.line.config.start});
        Object.defineProperty(this.startPoint, 'y', {get: () => this.fun(this.line.config.start)});
        Object.defineProperty(this.startPoint, 'title', {
            get: () => `${this.line.xs.config.titleHTML}=&nbsp${this.line.xs.createValue(this.line.config.start).display.format(3)} <br> ${this.line.ys.config.titleHTML}= ${this.line.ys.createValue(this.fun(this.line.config.start)).display.format(3)}`
        })
        ;

        this.endPoint = new GraphPoint(0, 0);
        this.endPoint.config = {projection: false};
        this.endPoint.titleAsSVG = true;
        this.endPoint.titleLines = 2;
        this._createCircle(this.endPoint);
        Object.defineProperty(this.endPoint, 'x', {get: () => this.line.config.end});
        Object.defineProperty(this.endPoint, 'y', {get: () => this.fun(this.line.config.end)});
        Object.defineProperty(this.endPoint, 'title', {get: () => `${this.line.xs.config.titleHTML}=&nbsp${this.line.xs.createValue(this.line.config.end).display.format(3)} <br> ${this.line.ys.config.titleHTML}= ${this.line.ys.createValue(this.fun(this.line.config.end)).display.format(3)}`});
    }

    _createCircle(point) {
        let circle = point.svgPoint.ownerDocument.createElementNS("http://www.w3.org/2000/svg", "circle");
        circle.setAttribute('cx', 0);
        circle.setAttribute('cy', 0);
        circle.setAttribute('r', 4);
        circle.style.fill = 'transparent';
        point.svgPoint.appendChild(circle);
    }

    /**
     * Wylicza punkty przecięcia lini odciętej
     * min i max są bardzo ważne, jeśli mamy funkcję inną niż wielomian (np. sprawność)
     * @param y pozycja osi odciętej
     * @param min minimalny zakres na osi X, który bierzemy pod uwagę
     * @param max maksymalny zakres na osi X
     * @returns {*|*}
     */
    getDataByY(y, min, max) {
        return this.polynomial.subtract(new Polynomial([y])).calcZeroPointsRange(min, max);
    }

}

module.exports.Line = Line;