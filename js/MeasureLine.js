var GraphPoint = require('graph/js/Point').Point;

/**
 * Linia rzednych/ociętych
 * Dziedziczy z point, ponieważ zachowuje się jak point, tylko wygląda inaczej
 * Można mieć kilka lini na różnych wykresach, które są ze sobą połączone: wtedy prZesuwają się równocześnie, i jak skasujemy którąś, to kasujemy wsystkie
 */
class MeasureLine extends GraphPoint {
    get color() {
        return this._color || '';
    }

    set color(value) {
        this._color = value;
        if (this._line)
            this._line.style.stroke = value;
    }

    /**
     * kasuje tą linie, i wsyzstkie linie połączone z nią na innych wykresach
     */
    remove() {
        if (!this.parentPlane)
            throw new Error('object is not on plane');
        if (!this.parentPlane.graphNetecs)
            throw new Error('object is on plane, that is not on GraphNetecs');

        for (let line of this._allLines) {
            if (line.parentPlane && line.parentPlane.graphNetecs)
                line.parentPlane.graphNetecs.removeMeasureLine(line);
        }
    }
}

module.exports.MeasureLine = MeasureLine;