const Constants = require('./Constants').Constants;

/**
 * Obiekt tej klasy zbiera wszystkie inputy, które wpisujemy w edycji pomiaru
 * W widoku edycji zczytuje te dane z inputów, w widoku podglądu zaczyutuje z bazy danych
 */
class Measurement {
    recalcMinMax() {
        let maxOnLimit = this.range_limit === this.range_max;//max jest równy limitowi, więc jak zmienimy limit to zmieniam też max
        if (maxOnLimit) {
            this.range_max = this.range_limit;
        } else {
            if (this.range_max > this.range_limit) {
                this.range_max = this.range_limit;
            }
            if (this.range_max < 0) {
                this.range_max = 0;
            }
        }
        if (this.range_min > this.range_max) {
            this.range_min = this.range_max;
        }
        if (this.range_min < 0) {
            this.range_min = 0;
        }
    }

    calcArea() {
        let area;
        if (this.outlet == 'rectangle') {
            let a = this.outlet_a / 1000;//na metry
            let b = this.outlet_b / 1000;//na metry
            area = a * b;
        } else {
            let diameter = this.outlet_diameter / 1000;//na metry
            let radius = diameter / 2;
            area = Math.PI * radius * radius;
        }
        this.outlet_area = area;
        return area;
    }

    calcInletArea() {
        let area;
        let diameter = this.avg_inlet / 1000;//na metry
        let radius = diameter / 2;
        area = Math.PI * radius * radius;
        this.inlet_area = area;
        return area;
    }

    /**
     * p_a
     */
    calcAtmosfericPressure() {
        return Constants.airPressure * Math.pow((1 - (6.5 * this.workPointAboveSea / 1000) / 288.15 /* NVS-179 this.workPointTemperature */), 5.256);
    }

    /**
     * \rho_a
     */
    calcAirDensity() {
        if (this.workPointAboveSea == 0 && this.workPointTemperature == 20 + 273.15)
            return Constants.airDensity;//defaultowa wartość
        else
            return (3.468 * this.calcAtmosfericPressure()) / (1000 * this.workPointTemperature);
    }

    limitValues() {
        if (this.workPointAirDensity < 0)
            this.workPointAirDensity = 0;
        if (this.workPointAmount < 0)
            this.workPointAmount = 0;
        if (this.workPointAmountUnit < 0)
            this.workPointAmountUnit = 0;
        if (this.workPointFrequency < 0.1)
            this.workPointFrequency = 0.1;
        if (this.workPointRotationSpeed < 6)
            this.workPointRotationSpeed = 6;
    }
}

module.exports.Measurement = Measurement;