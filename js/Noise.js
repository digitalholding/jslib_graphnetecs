var PolynomialLine = require('./PolynomialLine').PolynomialLine;
var Constants = require('./Constants').Constants;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;

/**
 * Hałas
 */
class Noise extends PolynomialLine {
    constructor(name, coefficients = [0, 0, 0, 0],fan, number_of_fans = 1, createGraphLine = true) {
        super(name, coefficients, createGraphLine);
        this.density = Constants.airDensity;
        this.frequency = Constants.frequency;
        this.number_of_fans = number_of_fans;
    }

    /**
     * współczynnik używany przy przeliczaniu halasu
     * @returns {number}
     * @private
     */
    get _xRatio() {
        return Constants.frequency / this.frequency;
    }

    /**
     * Stała, ktyóra ejst dodawana do równaniu funkcji przy przeliczeniach
     * @returns {number}
     * @private
     */
    get _constantRecalc() {
        return 50 * Math.log10(this.frequency / Constants.frequency) + 20 * Math.log10(this.density / Constants.airDensity);
    }

    get polynomial() {
        return this.polynomialOriginal.multiplyXByScalar(this._xRatio).add(new Polynomial([this._constantRecalc]))
    }

    fun(x) {
        return this.polynomialOriginal.calc((x / this.number_of_fans) * this._xRatio) + this._constantRecalc;

    }

    calcMinMax(minX, maxX) {
        let minValue = null;
        let maxValue = null;

        let xPositions = [];

        let length = maxX - minX;
        let n = 5;

        let gap = length / (n - 1);
        let elementN = minX;

        xPositions.push(minX);

        for (let i = 0; i < n - 1; i++) {
            elementN += gap;
            xPositions.push(elementN);
        }

        for (let xPosition of xPositions) {
            let calculatedValue = this.fun(xPosition);

            if (!minValue || minValue > calculatedValue)
                minValue = calculatedValue;

            if (!maxValue || maxValue < calculatedValue)
                maxValue = calculatedValue;
        }

        return {min: minValue, max: maxValue};
    }
}

module.exports.Noise = Noise;