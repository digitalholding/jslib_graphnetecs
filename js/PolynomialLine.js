var Line = require('./Line').Line;
var GraphLine = require('graph/js/Line').Line;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;

/**
 * Charakterystyka (funckja), która jest wielomianem
 */
class PolynomialLine extends Line {
    constructor(name, coefficients = [0, 0, 0, 0], createGraphLine = true) {
        super(createGraphLine);
        this.name = name;
        /**
         * Wieloniam przed przelcizeniami
         * @type {Polynomial}
         */
        this.polynomialOriginal = new Polynomial(coefficients);
        this.line = new GraphLine(this.fun.bind(this), null, createGraphLine);
        this.line.config = {resolution: 3};//w celu poprawy wydajności
    }

    get polynomial() {
        return this.polynomialOriginal;
    }

    fun(x) {
        return this.polynomial.calc(x);
    }

    calcMinMax(minX, maxX) {
        return this.polynomial.findMinMax(minX, maxX);
    }
}

module.exports.PolynomialLine = PolynomialLine;