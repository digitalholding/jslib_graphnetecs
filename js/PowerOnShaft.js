var PolynomialLine = require('./PolynomialLine').PolynomialLine;
var Constants = require('./Constants').Constants;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;

/**
 * Moc na wale
 */
class PowerOnShaft extends PolynomialLine {
    constructor(name, coefficients = [0, 0, 0, 0], densityDenominator = Constants.airDensity, number_of_fans = 1, createGraphLine = true) {
        super(name, coefficients, createGraphLine);
        this.density = (x) => {
            return Constants.airDensity
        };

        if (Number(densityDenominator) === densityDenominator && densityDenominator % 1 !== 0)
            this.densityDenominator = densityDenominator;
        else
            this.densityDenominator = Constants.airDensity;

        this.frequency = Constants.frequency;
        this.number_of_fans = number_of_fans;
    }

    /**
     * Współczynnik używany do przeliczania
     * @returns {number}
     * @private
     */
    get _xRatio() {
        return Constants.frequency / this.frequency;
    }

    /**
     * Współczynnik używany do przeliczania
     * @returns {number}
     * @private
     */
    _yRatio(x) {
        return this.density(x) / this.densityDenominator * Math.pow(this.frequency / Constants.frequency, 3);
    }

    get polynomial() {
        let ret = this.polynomialOriginal.multiplyXByScalar(this._xRatio)
        return ret.multiply(new Polynomial([this._yRatio(0)]));
    }

    fun(x) {
        return (this.polynomialOriginal.calc((x / this.number_of_fans) * this._xRatio) * this._yRatio(x / this.number_of_fans)) * this.number_of_fans;
    }

    async getByY(y, min, max) {
        let points = []
        let accuracy = .001
        for(let x = min; x <= max; x += accuracy) {
            let val = this.fun(x)
            if(val <= y + accuracy) points.push({x: x, y: val})
            else break
        }
        let closest = points[points.length - 1]
        if( closest && Math.abs(y - this.fun(closest.x)) <= accuracy * 100) return closest.x
    }

    calcMinMax(minX, maxX) {
        let minValue = null;
        let maxValue = null;

        let xPositions = [];

        let length = maxX - minX;
        let n = 5;

        let gap = length / (n - 1);
        let elementN = minX;

        xPositions.push(minX);

        for (let i = 0; i < n - 1; i++) {
            elementN += gap;
            xPositions.push(elementN);
        }

        for (let xPosition of xPositions) {
            let calculatedValue = this.fun(xPosition);

            if (!minValue || minValue > calculatedValue)
                minValue = calculatedValue;

            if (!maxValue || maxValue < calculatedValue)
                maxValue = calculatedValue;
        }

        return {min: minValue, max: maxValue};
    }
}

module.exports.PowerOnShaft = PowerOnShaft;