const Constants = require('./Constants').Constants;

var Line = require('./Line').Line;
var GraphLine = require('graph/js/Line').Line;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;
const ObjectEvent = require('object-event').ObjectEvent;

/**
 * Ciśnienie statyczne
 */
class StaticPressure extends Line {
    constructor(name, totalPressure, area, number_of_fans = 1, aboveSea = null, units, createGraphLine = true) {
        super(createGraphLine);
        this.name = name;
        this.area = area;
        this.totalPressure = totalPressure;
        this.temperature = Constants.temperature;
        this.changedFrequency = ObjectEvent('changed');//event, który wywołuje się jak przeciągnę linię
        this.changedTemperature = ObjectEvent('changed'); // NVS-92
        this.line = new GraphLine(this.fun.bind(this), null, createGraphLine);
        this.workPointAboveSea = aboveSea;
        this.units = units;

        if(createGraphLine) {
            this.line.moved(event => this._lineMoved(event));
            this.startPoint.moved(event => this._lineMoved(event));
            this.endPoint.moved(event => this._lineMoved(event));
        }

        this.number_of_fans = number_of_fans;
    }

    _lineMoved(event) {
        if (this.changingFerequencyEnabled) {
            if (event.moveType == 'start') {
                this.old = this.clone();
                this.line.parentPlane.add(this.old.line);
                this.old.line.svg.style.strokeDasharray = 5;
            }
            this.moveToPoint(event.x, event.y);
            this.showMovingTooltip(event);
            this.changedFrequency.call(this);
            if (event.moveType == 'end') {//puszczamy mysz
                this._movingTooltip.style.display = 'none';
                this.line.parentPlane.remove(this.old.line);
                delete this.old;
            }
        }
        // NVS-92
        else if(this.changingTemperatureEnabled) {
            if (event.moveType == 'start') {
                this.old = this.clone();
                this.line.parentPlane.add(this.old.line);
                this.old.line.svg.style.strokeDasharray = 5;
            }
            this.moveToPointT(event.x, event.y);
            this.showMovingTooltipT(event);
            this.changedTemperature.call(this);
            if (event.moveType == 'end') {//puszczamy mysz
                this._movingTooltip.style.display = 'none';
                this.line.parentPlane.remove(this.old.line);
                delete this.old;
            }
        }
    }

    clone() {
        let ret = new StaticPressure(this.name, this.totalPressure.clone(), this.area);
        ret.density = this.density;
        ret.frequency = this.frequency;
        ret.temperature = this.temperature;
        ret._min = this._min;
        ret._max = this._max;
        ret.line.config = this.line.config;
        return ret;
    }

    /**
     * Z tablicy liczb znajdź tą, która jest najbliżej
     * @param x
     * @param arr
     * @returns {*}
     */
    findNearestNumber(x, arr) {
        let x1 = arr[0];
        for (let x0 of arr) {
            if (x0 > 0 && Math.abs(x - x0) < Math.abs(x - x1)) x1 = x0;
        }
        return x1;
    }

    /**
     * Przeciąganie myszą lini: obliczanie nowej częstotliwości
     * @param x_1
     * @param y_1
     */
    moveToPoint(x_1, y_1) {
        let startingPolynomial = this.polynomial;
        let movingPath = new Polynomial([0, 0, y_1 / Math.pow(x_1, 2)]);
        let x_0All = (startingPolynomial.subtract(movingPath).calcZeroPoints());
        if (x_0All.length) {
            let x_0 = this.findNearestNumber(x_1, x_0All);//mamy wielomian 3 stopnia, więc może wyjść wiele rozwiązań, więc bierzemy te najbliżej aktualnej częstotliwości
            let oldFrequency = this.frequency;
            this.frequency = Math.round(x_1 / x_0 * this.frequency * 10) / 10;
            x_0 = oldFrequency * x_1 / this.frequency;
            this.line.config = {start: x_1 / x_0 * this.line.config.start, end: x_1 / x_0 * this.line.config.end};
            this.line.parentPlane.draw();
        }
    }

    /**
     * Przeciąganie myszą lini: obliczanie nowej temperatury NVS-92
     * @param x_1
     * @param y_1
     */
    moveToPointT(x_1, y_1) {
        // temperatura wylicza się w zależności od zmiany gęstości powietrza, czyli to przeciąganie ma zmieniać gęstość
        let startingPolynomial = this.polynomial;
        let movingPath = new Polynomial([0, 0, y_1 / Math.pow(x_1, 2)]);
        let x_0All = (startingPolynomial.subtract(movingPath).calcZeroPoints());
        if (x_0All.length) {
            let x_0 = this.findNearestNumber(x_1, x_0All);
            let oldDensity = this.density;
            this.density = Math.round(x_1 / x_0 * this.density * 10000) / 10000;
            this.temperature = (3.468 * this.calcAtmosfericPressure()) / this.density / 1000;
            x_0 = oldDensity * x_1 / this.density;
            this.line.config = {start: x_1 / x_0 * this.line.config.start, end: x_1 / x_0 * this.line.config.end};
            this.line.parentPlane.draw();
        }
    }

    calcAtmosfericPressure() {
        return Constants.airPressure * Math.pow((1 - (6.5 * this.workPointAboveSea / 1000) / 288.15 /* NVS-179 this.temperature */), 5.256);
    }

    showMovingTooltip(event) {
        if (!this._movingTooltip) {
            this._movingTooltip = document.createElementNS("http://www.w3.org/2000/svg", 'text');
            this.line.parentPlane.svg.appendChild(this._movingTooltip);
            this._movingTooltip.style.textAnchor = 'end';
            this._movingTooltip.setAttribute('x', '-5');
            this._movingTooltip.setAttribute('y', '-20');
        }
        this._movingTooltip.style.display = '';
        this._movingTooltip.style.transform = `translate(${event.oryginalEvent.offsetX}px,${event.oryginalEvent.offsetY}px)`;
        this._movingTooltip.textContent = this.frequency + 'Hz';
    }

    // NVS-92
    showMovingTooltipT(event) {
        if (!this._movingTooltip) {
            this._movingTooltip = document.createElementNS("http://www.w3.org/2000/svg", 'text');
            this.line.parentPlane.svg.appendChild(this._movingTooltip);
            this._movingTooltip.style.textAnchor = 'end';
            this._movingTooltip.setAttribute('x', '-5');
            this._movingTooltip.setAttribute('y', '-20');
        }
        this._movingTooltip.style.display = '';
        this._movingTooltip.style.transform = `translate(${event.oryginalEvent.offsetX}px,${event.oryginalEvent.offsetY}px)`;
        this._movingTooltip.textContent = Math.round((this.temperature - 273.15) * Math.pow(10, this.units.temperature.list[0].decimal_places)) / Math.pow(10, this.units.temperature.list[0].decimal_places) + '°C';
    }

    get polynomial() {
        let coefficients = this.totalPressure.polynomial.coefficients.slice(0);//klonowanie
        coefficients[2] -= (this.density / Math.pow((this.area), 2)) / 2;
        return new Polynomial(coefficients);
    }

    get polynomialOryginal() {
        let coefficients = this.totalPressure.polynomialOriginal.coefficients.slice(0);//klonowanie
        coefficients[2] -= (Constants.airDensity / Math.pow((this.area), 2)) / 2;
        return new Polynomial(coefficients);
    }

    get _airDensity() {
        return this.density;
    }

    fun(x) {
        return this.totalPressure.fun(x) - (this.density / Math.pow((this.area), 2)) / 2 * (x / this.number_of_fans) * (x / this.number_of_fans);
    }

    calcMinMax(minX, maxX) {
        let minValue = null;
        let maxValue = null;

        let xPositions = [];

        let length = maxX - minX;
        let n = 5;

        let gap = length / (n - 1);
        let elementN = minX;

        xPositions.push(minX);

        for (let i = 0; i < n - 1; i++) {
            elementN += gap;
            xPositions.push(elementN);
        }

        for (let xPosition of xPositions) {
            let calculatedValue = this.fun(xPosition);

            if (!minValue || minValue > calculatedValue)
                minValue = calculatedValue;

            if (!maxValue || maxValue < calculatedValue)
                maxValue = calculatedValue;
        }

        return {min: minValue, max: maxValue};
    }

    getZeroPoint(min) {
        return this.polynomialOryginal.calcZeroPoints().sort((a, b) => a - b).find(a => a > min);
    }
}

module.exports.StaticPressure = StaticPressure;