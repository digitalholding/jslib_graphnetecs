const Constants = require('./Constants').Constants;

var Line = require('./Line').Line;
var GraphLine = require('graph/js/Line').Line;
var Polynomial = require('mathFunctions/js/Polynomial').Polynomial;

/**
 * Przyrost ciśnienia statycznego
 */
class StaticPressureIncrease extends Line {
    constructor(name, totalPressure, area, inlet_area, number_of_fans = 1, createGraphLine = true) {
        super(createGraphLine);
        this.name = name;
        this.totalPressure = totalPressure;
        this.area = area;
        this.inlet_area = inlet_area;
        this.line = new GraphLine(this.fun.bind(this), null, createGraphLine);
        this.number_of_fans = number_of_fans;
    }

    get polynomial() {
        let coefficients = this.totalPressure.polynomial.coefficients.slice(0);
        coefficients[2] -= ((this.density / (Math.pow(this.area, 2) * 2)) - (this.density / (Math.pow(this.inlet_area, 2) * 2)));
        return new Polynomial(coefficients);
    }

    get polynomialOryginal() {
        let coefficients = this.totalPressure.polynomialOriginal.coefficients.slice(0);
        coefficients[2] -= ((Constants.airDensity / (Math.pow(this.area, 2) * 2)) - (Constants.airDensity / (Math.pow(this.inlet_area, 2) * 2)))
        return new Polynomial(coefficients);
    }

    get _airDensity() {
        return this.density;
    }

    fun(x) {
        return this.totalPressure.fun(x) - ((this.density / (Math.pow(this.area, 2) * 2)) - (this.density / (Math.pow(this.inlet_area, 2) * 2))) * (x / this.number_of_fans) * (x / this.number_of_fans);
    }


    calcMinMax(minX, maxX) {
        let minValue = null;
        let maxValue = null;

        let xPositions = [];

        let length = maxX - minX;
        let n = 5;

        let gap = length / (n - 1);
        let elementN = minX;

        xPositions.push(minX);

        for (let i = 0; i < n - 1; i++) {
            elementN += gap;
            xPositions.push(elementN);
        }

        for (let xPosition of xPositions) {
            let calculatedValue = this.fun(xPosition);

            if (!minValue || minValue > calculatedValue)
                minValue = calculatedValue;

            if (!maxValue || maxValue < calculatedValue)
                maxValue = calculatedValue;
        }

        return {min: minValue, max: maxValue};
    }

    getZeroPoint(min) {
        return this.polynomialOryginal.calcZeroPoints().sort((a, b) => a - b).find(a => a > min);
    }
}

module.exports.StaticPressureIncrease = StaticPressureIncrease;