var MeasureLine = require('./MeasureLine').MeasureLine;
const ObjectEvent = require('object-event').ObjectEvent;

class VerticalMeasureLine extends MeasureLine {
    /**
     *
     * @param value
     * @param allLines tablica, w której są wsyzstkie linie ze sobą związane
     */
    constructor(value, allLines = []) {
        super(value, 0);
        this._allLines = allLines;
        allLines.push(this);
        this.changed = ObjectEvent('changed');
        this.moved(e => {
            this.x = e.x;
        });
    }

    get x() {
        return this._x;
    }

    set x(value) {
        if (this._allLines) {
            for (let line of this._allLines) {
                line._x = value;
                line.changed.call(line);
            }
        } else {//w konstruktorze
            this._x = value;
        }
    }

    _draw(xs, ys, width, height) {
        super._draw(xs, ys, width, height);

        if (!this._line) {
            this._line = document.createElementNS("http://www.w3.org/2000/svg", 'line');
            this._line.classList.add('measureLine');
            this._line.classList.add('verticalMeasureLine');
            this._line.style.stroke = this._color;
            this.svg.appendChild(this._line);
        }

        let xPX = xs.getPixels(this.x);

        this._line.setAttribute('x1', xPX);
        this._line.setAttribute('y1', ys.marginEnd);
        this._line.setAttribute('x2', xPX);
        this._line.setAttribute('y2', height - ys.marginStart);

    }
}

module.exports.VerticalMeasureLine = VerticalMeasureLine;