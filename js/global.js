/**
 * Konwertuje liczbe do stringa po polsku
 * Używa przecinka do ułamków
 * wyświetla po polsku +nieskończoność i -nieskończoność
 * NaN wyświetla jako nieobliczalne
 * @param digits ilość miejsc po przecinku
 * @returns {string}
 */
Number.prototype.format = function (digits = null) {
    if (isNaN(this))
        return "nieobliczalne";
    if (this == Number.POSITIVE_INFINITY)
        return "+nieskończoność";
    if (this == Number.NEGATIVE_INFINITY)
        return "-nieskończoność";
    let number = this.toPrecision(14);//żeby było równo z PHP
    if (digits !== null) {
        let rounding = Math.pow(.1, digits);
        number = (Math.round(this / rounding) * rounding).toFixed(digits);
    } else {
        number = number.replace(/(\.[0-9]*[1-9])0+$/, '$1').replace(/\.0*$/, '')
    }
    return number.replace('.', ',');
};
Number.prototype.roundTo = function (decimalPlaces = 0) {
    var factor = Math.pow(10, decimalPlaces);
    return Math.round(this * factor) / factor;
}

/**
 * przeciwieństwo metody format
 * parsuje stringa na liczbę
 * przyjmuje zarówno przecinki i kropki do ulamków
 * @param n
 * @returns {number}
 */
Number.parse = function (n) {
    if (typeof n == 'number')
        return n;
    if (n.toString().trim() == 'nieskończoność' || n.toString().trim() == '+nieskończoność')
        return Number.POSITIVE_INFINITY;
    if (n.toString().trim() == '-nieskończoność')
        return Number.NEGATIVE_INFINITY;
    return (n.replace(',', '.')) * 1;
};