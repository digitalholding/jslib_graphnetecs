var expect = require("chai").expect;
var mock = require('mock-require');
let GraphPlane = function () {
};
mock('graph/js/Plane', {Plane: GraphPlane});
global.window = {testing: true};
const {GraphNetecs} = require("../js/GraphNetecs");
describe("GraphNetecs", function () {


    describe("lines", function () {
        it("copy", function () {
            GraphPlane.prototype = {
                add(x) {
                }, remove(x) {
                }
            };
            let line1 = {line: {}, showStartEndPoints: false};
            let line2 = {line: {}, showStartEndPoints: false};
            let obj = new GraphNetecs();
            obj.addLine(line1);
            obj.addLine(line2);
            expect(obj.lines).to.be.deep.equal([line1, line2]);
            let copy1 = obj.lines;
            copy1.push('abcd');
            expect(obj.lines).to.be.deep.equal([line1, line2]);//bez zmian


            expect(obj.lines).to.be.not.equal(obj.lines);//copy needed
        });
    });
    describe("points", function () {
        it("copy", function () {
            GraphPlane.prototype = {
                add(x) {
                }, remove(x) {
                }
            };
            let point1 = {};
            let point2 = {};
            let obj = new GraphNetecs();
            obj.addPoint(point1);
            obj.addPoint(point2);
            expect(obj.points).to.be.deep.equal([point1, point2]);
            let copy1 = obj.points;
            copy1.push('abcd');
            expect(obj.points).to.be.deep.equal([point1, point2]);//bez zmian


            expect(obj.points).to.be.not.equal(obj.points);//copy needed
        });
    });
    describe("addLine - removeLine", function () {
        it("basic", function () {
            let added = [];
            let removed = [];
            GraphPlane.prototype = {
                add(x) {
                    added.push(x);
                }, remove(x) {
                    removed.push(x);
                }
            };

            let obj = new GraphNetecs();

            let line1 = {line: {id: 1}, showStartEndPoints: false};
            let line2 = {line: {id: 2}, showStartEndPoints: false};
            obj.addLine(line1);
            obj.addLine(line2);

            expect(obj._lines).to.be.deep.equal([line1, line2]);
            expect(added).to.be.deep.equal([line1.line, line2.line]);
            expect(removed).to.be.deep.equal([]);
            expect(obj.containsLine(line1)).to.be.equal(true);
            expect(obj.containsLine(line2)).to.be.equal(true);

            obj.removeLine(line1);

            expect(obj.lines).to.be.deep.equal([line2]);
            expect(added).to.be.deep.equal([line1.line, line2.line]);
            expect(removed).to.be.deep.equal([line1.line]);
            expect(obj.containsLine(line1)).to.be.equal(false);
            expect(obj.containsLine(line2)).to.be.equal(true);

            obj.removeLine(line2);

            expect(obj.lines).to.be.deep.equal([]);
            expect(obj.containsLine(line1)).to.be.equal(false);
            expect(obj.containsLine(line2)).to.be.equal(false);
        });
    });
    describe("addPoint - removePoint", function () {
        it("basic", function () {
            let added = [];
            let removed = [];
            GraphPlane.prototype = {
                add(x) {
                    added.push(x);
                }, remove(x) {
                    removed.push(x);
                }
            };

            let obj = new GraphNetecs();

            let point1 = {id: 1};
            let point2 = {id: 2};
            obj.addPoint(point1);
            obj.addPoint(point2);

            expect(obj.points).to.be.deep.equal([point1, point2]);
            expect(added).to.be.deep.equal([point1, point2]);
            expect(removed).to.be.deep.equal([]);
            expect(obj.containsPoint(point1)).to.be.equal(true);
            expect(obj.containsPoint(point2)).to.be.equal(true);

            obj.removePoint(point1);
            expect(obj.points).to.be.deep.equal([point2]);
            expect(added).to.be.deep.equal([point1, point2]);
            expect(removed).to.be.deep.equal([point1]);
            expect(obj.containsPoint(point1)).to.be.equal(false);
            expect(obj.containsPoint(point2)).to.be.equal(true);

            obj.removePoint(point2);

            expect(obj.points).to.be.deep.equal([]);
            expect(obj.containsPoint(point1)).to.be.equal(false);
            expect(obj.containsPoint(point2)).to.be.equal(false);
        });
    });

    describe("_setMinMaxY", function () {
        it("empty", function () {
            GraphPlane.prototype = {
                add(x) {
                }, remove(x) {
                },
                ys: {}
            };

            let obj = new GraphNetecs();
            obj._setMinMaxY();
            expect(obj.graph.ys.config).to.be.deep.equal({start: 0, end: 1});
        });
        it("basic", function () {
            GraphPlane.prototype = {
                add(x) {
                }, remove(x) {
                },
                ys: {}
            };

            let obj = new GraphNetecs();
            let line1 = {
                calcMinMax() {
                    return {min: 1, max: 5}
                }, chartScaling: true
            };
            let line2 = {
                calcMinMax() {
                    return {min: 2, max: 3}
                }, chartScaling: true
            };

            obj.addLine(line1);
            obj.addLine(line2);

            obj._setMinMaxY();
            expect(obj.graph.ys.config).to.be.deep.equal({start: 1, end: 5});
        });
    });
});