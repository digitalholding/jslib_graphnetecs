var expect = require("chai").expect;
const Measurement = require("../js/Measurement").Measurement;

describe("Measurement", function () {
    describe("calcAirDensity", function () {
        it("default", function () {
            let obj = new Measurement();
            obj.workPointAboveSea=0;
            obj.workPointTemperature=293.15;
            expect(obj.calcAirDensity()).to.be.equal(1.2);
        });
    });
});